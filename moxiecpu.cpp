//
// Created by onliv on 4/4/2018.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "moxiecpu.h"

u32 pc = 0;

u32 fp = 0, sp = 0;
u32 rx[] = {0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0,};

u32 swi = 0;
u32 cc = 0;

u32 spec[256];

moxie_t moxie;

void build_moxie (void *mem) {
    moxie.mem8  = (u8 *) mem;

    memset (spec, 0, 256);
}

void setpc (u32 val) {
    pc = val;
}

u32 getreg (u8 reg) {
    switch (reg) {
        case 0x0: return fp;
        case 0x1: return sp;
        case 0x2: case 0x3:
        case 0x4: case 0x5:
        case 0x6: case 0x7:
        case 0x8: case 0x9:
        case 0xA: case 0xB:
        case 0xC: case 0xD:
        case 0xE: case 0xF:
            return rx[reg - 2];
    }
}

void setreg (u8 reg, u32 val) {
    switch (reg) {
        case 0x0: fp = val; break;
        case 0x1: sp = val; break;
        case 0x2: case 0x3:
        case 0x4: case 0x5:
        case 0x6: case 0x7:
        case 0x8: case 0x9:
        case 0xA: case 0xB:
        case 0xC: case 0xD:
        case 0xE: case 0xF:
            rx[reg - 2] = val;
            break;
    }
}

u16 read16 (u32 addr) {
    return moxie.mem8[addr] | (moxie.mem8[addr + 1] << 8);
}

u32 read32 (u32 addr) {
    return (read16 (addr) | (read16 (addr + 2) << 16));
}

u32 imm32 () {
    return read32 (pc + 2);
}

i16 imm16s () {
    return (i16) read16 (pc + 2);
}

void write8 (u32 addr, u8 val) {
    moxie.mem8[addr] = val;
}

void write16 (u32 addr, u16 val) {
    moxie.mem8[addr] = val >> 8;
    moxie.mem8[addr + 1] = val << 8;
}

void write32 (u32 addr, u32 val) {
    moxie.mem8[addr] = val & 0xFF;
    moxie.mem8[addr + 1] = (val & 0xFF00) >> 8;
    moxie.mem8[addr + 2] = (val & 0xFF0000) >> 16;
    moxie.mem8[addr + 3] = (val & 0xFF000000) >> 24;
}

void jsr (u32 dest, u32 ret) {
    write32 ((sp - 8), ret);
    write32 ((sp - 12), fp);
    sp -= 12;
    fp = sp;
    pc = dest;
}

u32 get_swi () {
    u32 val = swi;
    swi = 0;
    return val;
}

u32 bcc (u16 inst, bool cond) {
    i32 offset = 0;
    if (cond) {
        offset = (inst & 0x3ff) << 1;
        if (offset & 0x400)
            offset |= 0xfffff800;
        pc += 2 + offset;
    } else
        pc += 2;
    return 0;
}

void trace (u8 opcode) {
    printf ("pc: %08X (%02X)\n", pc, opcode);
    printf ("sp: %08X, fp: %08X", sp, fp);
    for (int i = 0; i <= 13; i++) {
        if (i % 5 == 0)
            printf ("\n");
        printf ("r%X: %08X | ", i, rx[i]);
    }

    printf ("\n\n");
}

typedef union form1 {
    struct {
        u8 b : 4;
        u8 a : 4;
        u8 opcode;
    };
    u16 inst;
} form1_t;

u32 step () {
    u16 inst = read16 (pc);
    form1_t f1 = {
        .inst = inst,
    };
    u8 a, b;
    u32 va, vb;
    u32 imm;
    i32 imms;

    switch (inst >> 8) {
        case 0x00:
            return SIGILL;
        case 0x01: // ldi.l
            setreg (f1.a, imm32 ());
            pc += 6;
            break;
        case 0x02: // mov
            setreg (f1.a, getreg (f1.b));
            pc += 2;
            break;
        case 0x03: // jsra
            imm = imm32();
            jsr (imm, pc + 6);
            break;
        case 0x04: // ret
            sp = fp;
            fp = read32 (sp);
            pc = read32 (sp + 4);
            sp += 12;
            break;
        case 0x05: // add.l
            setreg (f1.a, (getreg (f1.a) + getreg (f1.b)));
            pc += 2;
            break;
        case 0x08: // lda.l
            setreg (f1.a, read32 (imm32()));
            pc += 6;
            break;
        case 0x0a: // ld.l
            setreg (f1.a, read32 (getreg (f1.b)));
            pc += 2;
            break;
        case 0x0b: // st.l
            write32 (getreg (f1.a), getreg (f1.b));
            pc += 2;
            break;
        case 0x0c: // ldo.l
            imms = imm16s ();
            setreg (f1.a, read32 ((getreg (f1.b) + imms)));
            pc += 4;
            break;
        case 0x0d: // sto.l
            imms = imm16s ();
            write32 ((getreg (f1.a) + imms), getreg (f1.b));
            pc += 4;
            break;
        case 0x0e: // cmp
            {
                va = getreg (f1.a);
                vb = getreg (f1.b);
                i32 sa = (i32) va;
                i32 sb = (i32) vb;
                if (va == vb)
                    cc = CC_EQ;
                else {
                    cc = 0;
                    if (sa < sb)
                        cc |= CC_LT;
                    if (sa > sb)
                        cc |= CC_GT;
                    if (va < vb)
                        cc |= CC_LTU;
                    if (va > vb)
                        cc |= CC_GTU;
                }
                pc += 2;
                break;
            }
        case 0x0f: // nop
            pc += 2;
            break;
        case 0x12: // zex.b
            setreg (f1.a, getreg (f1.b) & 0xff);
            pc += 2;
            break;
        case 0x1a: // jmpa
            pc = imm32 ();
            break;
        case 0x1b: // ldi.b
            imm = imm32 ();
            setreg (f1.a, imm & 0xFF);
            pc += 6;
            break;
        case 0x1c: // ld.b
            setreg (f1.a, moxie.mem8[getreg (f1.b)]);
            pc += 2;
            break;
        case 0x1e: // st.b
            write8 (getreg (f1.a), getreg (f1.b));
            pc += 2;
            break;
        case 0x26: // and
            setreg (f1.a, getreg (f1.a) & getreg (f1.b));
            pc += 2;
            break;
        case 0x27: // lshr
            setreg (f1.a, getreg (f1.a) >> getreg (f1.b));
            pc += 2;
            break;
        case 0x28: // ashl
            setreg (f1.a, getreg (f1.a) << getreg (f1.b));
            pc += 2;
            break;
        case 0x29: // sub.l
            setreg (f1.a, (getreg (f1.a) - getreg (f1.b)));
            pc += 2;
            break;
        case 0x2E: // xor
            setreg (f1.a, getreg (f1.a) ^ getreg (f1.b));
            pc += 2;
            break;
        case 0x2F: // mul
            setreg (f1.a, (i32) getreg (f1.a) * (i32) getreg (f1.b));
            pc += 2;
            break;
        case 0x30: // swi
            swi = imm32 ();
            if (!swi)
                return SIGILL;
            spec[SR_SWI_N] = swi;
            pc += 6;
            break;
        case 0x31: // div.l
            setreg (f1.a, (i32) getreg (f1.a) / (i32) getreg (f1.b));
            pc += 2;
            break;
        case 0x32: // udiv.l
            setreg (f1.a, getreg (f1.a) / getreg (f1.b));
            pc += 2;
            break;
        case 0x34: // umod.l
            setreg (f1.a, getreg (f1.a) % getreg (f1.b));
            pc += 2;
            break;
        case 0x35: // brk
            pc += 2;
            exit(0);
            return SIGTRAP;
        case 0x36: // ldo.b
            imms = imm16s ();
            setreg (f1.a, moxie.mem8[(getreg (f1.b) + imms)]);
            pc += 4;
            break;
        case 0x37: // sto.b
            imms = imm16s ();
            write8 ((getreg (f1.a) + imms), (u8) getreg (f1.b));
            pc += 4;
            break;
        case 0x80: case 0x81: case 0x82: case 0x83:
        case 0x84: case 0x85: case 0x86: case 0x87:
        case 0x88: case 0x89: case 0x8A: case 0x8B:
        case 0x8C: case 0x8D: case 0x8E: case 0x8F:
            // inc
            a = (u8) ((inst >> 8) & 0xf);
            va = getreg (a);
            setreg (a, (va + (inst & 0xFF)));
            pc += 2;
            break;
        case 0x90: case 0x91: case 0x92: case 0x93:
        case 0x94: case 0x95: case 0x96: case 0x97:
        case 0x98: case 0x99: case 0x9A: case 0x9B:
        case 0x9C: case 0x9D: case 0x9E: case 0x9F:
            // dec
            a = (u8) ((inst >> 8) & 0xf);
            va = getreg (a);
            setreg (a, (va - (inst & 0xFF)));
            pc += 2;
            break;
        case 0xc0: case 0xc1: case 0xc2: case 0xc3: // beq
            return bcc (inst, cc & CC_EQ);
        case 0xc4: case 0xc5: case 0xc6: case 0xc7: // bne
            return bcc (inst, !(cc & CC_EQ));
        case 0xc8: case 0xc9: case 0xca: case 0xcb: // blt
            return bcc (inst, cc & CC_LT);
        case 0xcc: case 0xcd: case 0xce: case 0xcf: // bgt
            return bcc (inst, cc & CC_GT);
        case 0xd0: case 0xd1: case 0xd2: case 0xd3: // bltu
            return bcc (inst, cc & CC_LTU);
        case 0xd4: case 0xd5: case 0xd6: case 0xd7: // bgtu
            return bcc (inst, cc & CC_GTU);
        case 0xd8: case 0xd9: case 0xda: case 0xdb: // bge
            return bcc (inst, cc & (CC_GT | CC_EQ));
        case 0xdc: case 0xdd: case 0xde: case 0xdf: // ble
            return bcc (inst, cc & (CC_LT | CC_EQ));
        case 0xe0: case 0xe1: case 0xe2: case 0xe3: // bgeu
            return bcc (inst,cc & (CC_GTU | CC_EQ));
        case 0xe4: case 0xe5: case 0xe6: case 0xe7: // bleu
            return bcc (inst,cc & (CC_LTU | CC_EQ));
        default:
            printf ("Unimplemented: 0x%02x @ %04X", inst >> 8, pc);
            return SIGILL;
    }
    return 0;
}
