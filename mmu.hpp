#ifndef MMU_H
#define MMU_H

#include <stdint.h>
#include <stdbool.h>
#include <vector>

#include "defs.h"
#include "stdio.h"

typedef u8 (* r8_t) (m_ptr);

typedef void (* w8_t) (m_ptr, u8 val);

typedef struct mem_map {
    m_ptr start;
    m_ptr end;

    r8_t read8;

    w8_t write8;

    bool readonly = false;

    struct mem_map *prev;
    struct mem_map *next;
} mem_map_t;

static constexpr bool map_contains_addr (mem_map_t *map, m_ptr addr) {
    return addr >= map->start && addr < map->end;
}

static constexpr u8 get_map_addr (mem_map_t *map, m_ptr addr) {
    return addr - map->start;
}

class memory_unit {
public:
    memory_unit ();

    u8 read8 (m_ptr addr);
    u16 read16 (m_ptr addr);
    u32 read32 (m_ptr addr);

    bool write8  (m_ptr addr, u8  val);
    bool write16 (m_ptr addr, u16 val);
    bool write32 (m_ptr addr, u32 val);

    void insert_map (mem_map_t *map);
    mem_map_t *get_map (m_ptr addr);
private:
    mem_map_t *root_map;
};

extern u16 read16 (m_ptr addr);
extern u32 read32 (m_ptr addr);

extern void write8  (m_ptr addr, u8  val);
extern void write16 (m_ptr addr, u16 val);
extern void write32 (m_ptr addr, u32 val);

#endif
