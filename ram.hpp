#ifndef RAM_H
#define RAM_H

#include "defs.h"

class RAM : public hardware {
public:
    RAM (u32 size);
private:
    u8 *ram;

    u8 read8 (m_ptr addr);

    void write8  (m_ptr addr, u8  val);
};

#endif
