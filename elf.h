//
// Created by onliv on 4/4/2018.
//

#ifndef MOXIE_ELF_H
#define MOXIE_ELF_H

#define ELF_MAG0 "\x7f"
#define ELF_MAG1 "\x45"
#define ELF_MAG2 "\x4c"
#define ELF_MAG3 "\x46"

#define ELFCLASS32 1

#define ELFDATA2LSB 1
#define ELFDATA2MSB 2

#define EM_MOXIE 223

#define PT_LOAD 1

#endif //MOXIE_ELF_H
