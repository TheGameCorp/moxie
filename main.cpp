#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <thread>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include <SDL2/SDL.h>

#include "elf.h"
#include "defs.h"
#include "moxiecpu.h"
#include "SDLWindow.h"
#include "mmu.hpp"

typedef u16 (*r16_elf_t)(u8 *, m_ptr);
typedef u32 (*r32_elf_t)(u8 *, m_ptr);

u16 r16le(u8 *arr, m_ptr i)
{
    return arr[i] | (arr[i + 1] << 8);
}

u32 r32le(u8 *arr, m_ptr i)
{
    return arr[i] | (arr[i + 1] << 8) | (arr[i + 2] << 16) | (arr[i + 3] << 24);
}

u16 r16be(u8 *arr, m_ptr i)
{
    return (arr[i] << 8) | arr[i + 1];
}

u32 r32be(u8 *arr, m_ptr i)
{
    return (arr[i] << 24) | (arr[i + 1] << 16) | (arr[i + 2] << 8) | arr[i + 3];
}

typedef struct read
{
    r16_elf_t r16;
    r32_elf_t r32;
} read_t;

read_t readLE = {
    .r16 = &r16le,
    .r32 = &r32le,
};

read_t readBE = {
    .r16 = &r16be,
    .r32 = &r32be,
};

#include <stdint.h>
#if defined(__linux)
#define HAVE_POSIX_TIMER
#include <time.h>
#ifdef CLOCK_MONOTONIC
#define CLOCKID CLOCK_MONOTONIC
#else
#define CLOCKID CLOCK_REALTIME
#endif
#elif defined(__APPLE__)
#define HAVE_MACH_TIMER
#include <mach/mach_time.h>
#elif defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
static uint64_t ns()
{
    static uint64_t is_init = 0;
#if defined(__APPLE__)
    static mach_timebase_info_data_t info;
    if (0 == is_init)
    {
        mach_timebase_info(&info);
        is_init = 1;
    }
    uint64_t now;
    now = mach_absolute_time();
    now *= info.numer;
    now /= info.denom;
    return now;
#elif defined(__linux)
    static struct timespec linux_rate;
    if (0 == is_init)
    {
        clock_getres(CLOCKID, &linux_rate);
        is_init = 1;
    }
    uint64_t now;
    struct timespec spec;
    clock_gettime(CLOCKID, &spec);
    now = spec.tv_sec * 1.0e9 + spec.tv_nsec;
    return now;
#elif defined(_WIN32)
    static LARGE_INTEGER win_frequency;
    if (0 == is_init)
    {
        QueryPerformanceFrequency(&win_frequency);
        is_init = 1;
    }
    LARGE_INTEGER now;
    QueryPerformanceCounter(&now);
    return (uint64_t)((1e9 * now.QuadPart) / win_frequency.QuadPart);
#endif
}

using websocketpp::lib::bind;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

typedef websocketpp::server<websocketpp::config::asio> server;

void on_message(server *s, websocketpp::connection_hdl hdl, server::message_ptr msg)
{
    std::cout << msg->get_payload() << std::endl;
}

int main(int argc, char **argv)
{
    /*server print_server;

    print_server.set_message_handler(bind(&on_message, &print_server, ::_1, ::_2));

    print_server.init_asio();
    print_server.listen(5000);
    print_server.start_accept();

    print_server.run();*/

    mem_map_t *map = (mem_map_t *)malloc((sizeof(mem_map_t)));
    map->start = 0x0000;
    map->end = 0xFFFF;
    map->next = nullptr;
    mem_map_t *map2 = (mem_map_t *)malloc((sizeof(mem_map_t)));
    map2->start = 0x0100;
    map2->end = 0x0200;
    map2->next = nullptr;

    printf("First map: %p\n", map);
    printf("Second map: %p\n\n", map2);

    memory_unit *mmu = new memory_unit();
    mmu->insert_map(map);

    printf("Map at 0x0000: %p\n", mmu->get_map(0x0000));
    mmu->insert_map(map2);
    printf("Map at 0x0100: %p\n", mmu->get_map(0x0100));
    printf("Map at 0x0200: %p\n", mmu->get_map(0x0200));
    printf("Map at 0x0500: %p\n", mmu->get_map(0x0500));

    /*if (mmu->get_map (0xFFFFF) == nullptr) {
        printf ("Overflow works");
    }*/

    /*SDLWindow *window;
    std::thread ([&] {
        window = new SDLWindow (640, 400, "Moxie");
        window->EnterLoop (NULL, NULL);
    }).detach ();

    setbuf(stdout, NULL);
    FILE *file;
    char *data8;
    size_t file_len;

    u8 *mem = (u8 *) malloc (0x20000000);
    memset (mem, 0, 0x20000000);

    file = fopen (argv[1], "rb");
    assert (file != NULL);

    fseek (file, 0, SEEK_END);
    file_len = (size_t) ftell (file);
    fseek (file, 0, SEEK_SET);

    data8 = (char *) malloc (file_len + 1);
    fread (data8, file_len, 1, file);
    fclose (file);

    for (i32 i = 0; i < 4; i++)
        assert (data8[i] == (ELF_MAG0 ELF_MAG1 ELF_MAG2 ELF_MAG3)[i]);
    assert (data8[4] == ELFCLASS32);

    read_t r;

    switch (data8[5]) {
        case ELFDATA2LSB:
            r = readLE;
            break;
        case ELFDATA2MSB:
            r = readBE;
            break;
    }

    assert (r.r16 ((u8 *) data8, 18) == EM_MOXIE);

    u32 e_entry = r.r32 ((u8 *) data8, 24);
    u32 e_phoff = r.r32 ((u8 *) data8, 28);
    u16 e_phentsize = r.r16 ((u8 *) data8, 42);
    u16 e_phnum = r.r16 ((u8 *) data8, 44);

    for (i32 i = 0; i < e_phnum; i++) {
        u32 off = e_phoff + i * e_phentsize;
        u32 p_type = r.r32 ((u8 *) data8, off);
        u32 p_offset = r.r32 ((u8 *) data8, off + 4);
        u32 p_vaddr = r.r32 ((u8 *) data8, off + 8);
        u32 p_filesz = r.r32 ((u8 *) data8, off + 16);
        u32 p_memsz = r.r32 ((u8 *) data8, off + 20);

        if (p_type != PT_LOAD)
            continue;
        assert (p_filesz <= p_memsz);
        memcpy (&mem[p_vaddr], &data8[p_offset], p_filesz);
        for (i32 j = p_filesz; j < p_memsz; ++j)
            mem[p_vaddr + i] = 0;
    }

    setpc (e_entry);
    build_moxie (mem);

    while (window != nullptr ? !window->IsCloseRequested () : true) {
        u32 signal = step ();
        if (signal == SIGILL || signal == SIGTRAP)
            break;

        switch (get_swi ()) {
            case 0x01:
                putchar (rx[0]);
                break;
        }
    }

    free (data8);*/

    return 0;
}
