#include "mmu.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

memory_unit::memory_unit () :
    root_map (nullptr) {

}

void memory_unit::insert_map (mem_map_t *map) {
    if (this->root_map == nullptr)
        this->root_map = map;
    else {
        mem_map_t *head = this->get_map (map->start);

        if (map->end < head->end) {
            mem_map_t *tail = (mem_map_t *) malloc ((sizeof (mem_map_t)));
            memcpy (tail, head, sizeof (mem_map_t));

            tail->start = map->end;
            tail->end = head->end;

            tail->prev = map;
            tail->next = head->next;

            map->next = tail;
        }

        head->end = map->start;
        head->next = map;
    }
}

mem_map_t *memory_unit::get_map (m_ptr addr) {
    mem_map_t *map = this->root_map;

    do {
        if (map_contains_addr (map, addr))
            break;
        map = map->next;
    } while (map != nullptr);

    return map;
}

u8 memory_unit::read8 (m_ptr addr) {
    return 0;
}