#include "SDLWindow.h"

SDLWindow::SDLWindow (int width, int height, const std::string title) :
    width (width),
    height (height),
    title (title),
    is_close_requested (false) {
    SDL_Init (SDL_INIT_VIDEO);

    this->window = SDL_CreateWindow (this->title.c_str (), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, this->width, this->height, (uint32_t) NULL);
    this->renderer = SDL_CreateRenderer (this->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

SDLWindow::~SDLWindow () {
    SDL_DestroyRenderer (this->renderer);
    SDL_DestroyWindow (this->window);
}

void SDLWindow::Update (std::function<void ()> func) {
    SDL_Event e;
    while (SDL_PollEvent (&e)) {
        if (e.type == SDL_QUIT) {
            this->is_close_requested = true;
            printf ("Close\n");
        }
    }

    if (func != NULL)
        func ();
}

void SDLWindow::Render (std::function<void (SDL_Renderer *)> func) {
    this->Clear ();
    if (func != NULL)
        func (this->GetRenderer ());
    this->Present ();
}

void SDLWindow::EnterLoop (std::function<void ()> update, std::function<void (SDL_Renderer *)> renderer) {
    while (!this->IsCloseRequested ()) {
        Update (update);
        Render (renderer);
    }
}
