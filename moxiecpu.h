//
// Created by onliv on 4/4/2018.
//

#ifndef MOXIE_MOXIECPU_H
#define MOXIE_MOXIECPU_H
#include "defs.h"
#include "mmu.hpp"

#include <stdbool.h>
#include <vector>

#define SIGILL 4
#define SIGTRAP 5

#define CC_GT 0x01
#define CC_LT 0x02
#define CC_EQ 0x04
#define CC_GTU 0x08
#define CC_LTU 0x10

enum moxie_spec_reg {
    SR_STATUS = 0,
    SR_EXCEP_PTR,
    SR_EXCEP_VAL,
    SR_SWI_N,
    SR_SUPER_STACK,
    SR_EXCEPT_RET_ADDR,
    SR_RESERVED_0,
    SR_RESERVED_1,
    SR_RESERVED_2,
    SR_DEV_TREE,

    SR_SYSCALL = 0x80,
};

typedef struct moxie {
    u8 *mem8;
    memory_unit *mmu;
} moxie_t;

extern moxie_t moxie;

extern u32 pc;
extern u32 cc;

extern u32 fp, sp;
extern u32 rx[];

extern u32 spec[];

extern u32 swi;

extern void build_moxie (void *mem);

extern u32 getreg (u8 reg);
extern void setreg (u8 reg, u32 val);

extern u32 get_swi ();

extern void setpc (u32 val);

extern u32 step_improved ();
extern u32 step ();

extern u32 imm32 ();
extern i16 imm16s ();

extern u32 bcc (u16 inst, bool cond);
extern void jsr (u32 dest, u32 ret);

#endif //MOXIE_MOXIECPU_H
